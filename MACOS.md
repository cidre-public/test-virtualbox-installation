# Install UTM to run a .qcow2

This tutorial helps you to have the VMs of the Network and Security course running under MACOS. It has been possible thanks to Frédéric Tronel and Erwan Fasquel.

We converted the Virtualbox images to qcow2 images that can run using UTM. It can be slow (because hardware instructions are emulated) but our tests show that it work fine (at least on a Mac Mini).

The converted images are:

- [TestVM](https://nextcloud.centralesupelec.fr/s/cXz325pRABgMWMs): a small image to [test your installation](https://gitlab-research.centralesupelec.fr/cidre-public/test-virtualbox-installation/-/blob/master/README.md).
- [TP2](https://nextcloud.centralesupelec.fr/s/JsAbFbL7dKxWooZ): deployment of a VPN
- [TP3](https://nextcloud.centralesupelec.fr/s/Zo2PSYsyNYgyPxs): web application security

## Download UTM

Run

    $ brew install --cask utm

or download .dmg at https://mac.getutm.app

## Install and emulate .qcow2

### Example with TestVM.qcow2

1. Press + Button

![](./fig/fig1_open_menu.png)

2. Select Emulate

![](./fig/fig2_emulate.png)

3. Select other

![](./fig/fig3_other.png)

4. Boot device to None and click Continue

![](./fig/fig4_boot_device_none.png)

5. Select VM memory, storage and shared directory

![](./fig/fig5_select_vm_memory.png)

![](./fig/fig6_select_storage.png)

![](./fig/fig7_select_shared_dir.png)

6. Set name and click Save

![](./fig/fig8_select_name.png)

7. Right click on the added machine and select Edit

![](./fig/fig9_open_edit.png)

8. Uncheck UEFI Boot

![](./fig/fig10_uncheck_uefi_boot.png)

9. Select New Drive, Interface is IDE, Size is the size of the .qcow2 file, and then click Import

![](./fig/fig11_select_new_drive.png)

10. A modal will open, select the .qcow2 file you created and press Open

![](./fig/fig12_add_drive.png)

11. Drag the new IDE Drive to the top and click Save

![](./fig/fig13_drag_ide_top.png)

12. Run

![](./fig/fig14_run.png)

13. Exec `evaluated.sh`

![](./fig/fig15_exec.png)

## TP2
Same install as above. I tested the following configuration:
* Memory: 4GB
* Storage: 10GB
* Size (for IDE Drive): 10GB

![](./fig/fig17_config_tp2.png)

### TP3

Same install as above. I tested the following configuration:
* Memory: 4GB
* Storage: 24GB
* Size (for IDE Drive): 10GB

![](./fig/fig16_config_tp3.png)

## References

* https://simo9265.medium.com/convert-ova-to-qcow2-and-start-it-with-utm-13fa3fc4c3db
