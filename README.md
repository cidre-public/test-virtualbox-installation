# Test your Virtualbox Installation

This page helps you to test if your VirtualBox install is ok.

**WARNING**: If you avec MAC computer, please look at this [page](MACOS.md).

## Install VirtualBox

If not already done, install VirtualBox on your computer. You can follow the instructions given [here](http://student-pc.centralesupelec.fr/fr/node/332) by the DISI.

## Test your installation

For testing VirtualBox, we have developed a small virtual image that should run smoothly on your computer. Follow the following steps:

- Download TestVM.ova from this [link](https://centralesupelec-my.sharepoint.com/:u:/g/personal/jean-francois_lalande_centralesupelec_fr/EYVtYTXrPhlIhhllx6bRyccB8_jP1fN_0mJcBYjs-wVkhg?e=ydISur). For the ones that are under Mac, please see [here](MACOS.md).

- Import TestVM.ova: Menu File > import appliance, then select TestVM.ova and hit next and finalize hitting "Import" - ([help](https://docs.oracle.com/cd/E26217_01/E26796/html/qs-import-vm.html))

![Import the VM](import.png)

- You now see the TestVM virtual image on the left
- Select it and hit the "Start" button

![Start the VM](start.png)

- Wait and look at the screen: when the virtual machine has finished to boot, you should obtain the following result:

![The result that should be obtained](evaluate.png)

If some tests fail, please find your problem below and a possible solution.

- Shutdown the VM

![Shutdown the VM](shutdown.png)

You are now ready with your VirtualBox installation :)

## Problems and solutions

### The VM does not start and Virtualbox displays an error

#### VT-X is missing

![VT-X is missing](vtx_not_installed.png)

You should enable the intel VT-x instructions in your BIOS.

In windows 10, you should:

- go to the update screen of Windows
- reboot into the BIOS
- activate intel VT-x

This [page](https://www.shaileshjha.com/step-by-step-guide-to-enable-intel-vt-x-or-amd-v-in-bios-or-uefi-in-windows-10-and-windows-8/) should help you.

In windows 11, extra steps could be necessary:

- Turn off Hyper-V
- Disable Credential Guard
- Turn off hypervisor in the boot loader
- Disable DeviceGuard
- Disable Memory Integrity

This [page](https://answers.microsoft.com/en-us/surface/forum/all/how-to-enable-vt-x-on-surface-book-3/b9057c59-eb8a-4221-85c4-90a3d44edd55) should help you.

#### Error: vboxnet0 (adapter 2)

![vboxnet0 missing](private_network_missing.png)

The VM does not start because the private network vboxnet0 have not been created.

- Go to File > Host network manager
- Create the vboxnet0 network using the Create button

![create vboxnet0](create_vboxnet0.png)

You should have two network adapters at the end:

- One Host-only (vboxnet0)
- One NAT network (NatNetwork)

as shown below:

![vboxnet0 host ony](host-only-vboxnet0.png)

![nat](nat-network.png)


#### Error: Failed to create a host network interface

![error creating vboxnet0](error_vboxnet0.png)

It seems that the installation of VirtualBox was done with wrong options. Try to uninstall VirtualBox and install it again. It should solve this problem.

#### USB problem

You should install the extension pack. Go on [this page](https://www.virtualbox.org/wiki/Downloads) and click the VirtualBox X.Y.Z Oracle VM VirtualBox Extension Pack and follow the instructions.

### The VM starts and it shows an error

#### Pinging 8.8.8.8 fails

Your VM cannot access internet. It is probably due to your computer that is not connected to internet or your traffic is filtered. Try to reconnect to internet or change of network provider and recheck.

#### DNS resolution fails

If you can ping 8.8.8.8 is ok and the DNS resolution fails, it is a strange problem. It means that the DNS server embedded in VirtualBox cannot send DNS request outside. It should not happen. Try to change of network provider.

#### DHCP is blocked and the test does not continue

![dhcm is blocked](dhcp_blocked.png)

This is because your virtual network card has been disabled or misconfigured. Go to "Paramètres", then "Réseau" and activate the network card in NAT mode:

![eth0](eth0.png)

#### There is an error about eth1

![eth1 is not there](eth1_not_found.png)

Probably, you have misconfigured the second network card: this network card is connected to the private host vboxnet0. You should configure it as follows (Paramètres > Réseau):

![eth1](eth1.png)

If vboxnet0 is missing go to the previous error "vboxnet0 is missing".

#### Error about the Virtualbox Host-Only ethernet address

This error may occur under windows, probably because the name of your adapter is not vboxnet0 and has a different name. Under Windows, your network adapters can look like this:

![network manager](manager_network.png)

In this case, you have to check the parameters of the virtual machine:

- select the TestVM virtual machine
- hit the "Settings" button
- Go to the network tab
- Check the adapter 1 and adapter 2

  - adapter 1 should have the mode "NAT"
  - adapter 2 shoudl have the mode "host only network" with name "vboxnet0" or "VirtualBox Host-Only Ethernet Adapter"

![adapter 1](adapter1.png)

![adapter 2](adapter2.png)

Validate the configuration and relaunch the VM.

### Other problems

#### Cannot exit the mouse from the VM

By default the key "Ctrl" on the right, asks Virtualbox to exit from the VirtualBox window. If your keyboard has no right Ctrl key, you should customize the key in the preferences of Virtualbox.

#### The screen of the VM is very small

It is really difficult to read the screen !

You can increase the screen size by going into the menu:

Screen > Virtual screen number 1 > Resize to 125%

